import { Box, Grid } from "@material-ui/core"


export default function NewsItem(props) {
    const imgStyle = {
        "max-width": "100%"
    };

    const sizeParams = {
        "small": {
            titleFontSize: "h6.fontSize",
            synposisFontSize: 14,
            imageGridSize: 5
        },
        "normal": {
            titleFontSize: "h5.fontSize",            
            synposisFontSize: 16,
            imageGridSize: 5
        },
        "large": {
            titleFontSize: "h4.fontSize",
            synposisFontSize: 24,
            imageGridSize: 7
        }
    }

    if (props.news) {
        return (
            <div>
                <a href={props.news.link}>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <Box fontWeight={500} fontSize={sizeParams[props.size].titleFontSize}>{props.news.title}</Box>
                        </Grid>
                        <Grid item xs={sizeParams[props.size].imageGridSize}>
                            <img src={props.news.image.imgSrc} alt={props.news.image.altText} style={imgStyle} />
                        </Grid>
                        <Grid item xs>
                            <Box fontSize={sizeParams[props.size].synposisFontSize}>{props.news.synopsis}</Box>
                        </Grid>
                    </Grid>
                </a>
            </div>

        )
    }
    return (
        <div>Error</div>
    )
  }
  
import NewsItem from './newsItem';

import { Grid, useMediaQuery, useTheme } from '@material-ui/core';

export default function NewsContainer(props) {
    const theme = useTheme();
    const matches = useMediaQuery(theme.breakpoints.only('xs'));
    
    if (props.data) {
        return (
            <Grid container spacing={5}>
                <Grid item xs={12} sm={8}>
                    { matches }
                    <NewsItem 
                        news={props.data[0]} 
                        size={ matches ? "small" : "large" }    
                    />
                </Grid>
                <Grid item xs={12} sm={4}>
                    <Grid container spacing={5}>
                        <Grid item xs={12}>
                            <NewsItem 
                                news={props.data[1]}
                                size="small"
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <NewsItem 
                                news={props.data[2]}
                                size="small"
                            />
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item xs={12} sm={6}>
                    <NewsItem 
                        news={props.data[3]}
                        size={ matches ? "small" : "normal" }
                    />
                </Grid>
                <Grid item xs={12} sm={6}>
                    <NewsItem
                        news={props.data[4]}
                        size={ matches ? "small" : "normal" }
                    />
                </Grid>
            </Grid>
        )
    }
    return (
        <div>Loading...</div>
    )
}

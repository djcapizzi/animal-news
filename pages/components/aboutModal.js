import { Modal, makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
    paper: {
      position: 'absolute',
      width: 400,
      backgroundColor: theme.palette.background.paper,
      border: '2px solid #000',
      boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 4, 3),
      top: '35%',
      left: '50%',
      transform: 'translate(-50%, -50%)',
    },
  }));


export default function AboutModal(props) {
    const classes = useStyles();

    return (
        <Modal
            open={props.open}
            onClose={props.handleClose}
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
        >
            <div className={classes.paper}>
                <h2 id="simple-modal-title">What is this?</h2>
                <p id="simple-modal-description">
                    Furry Feed &#128062; serves up a random selection of good news about animals to you, aggregated from news sources all over the web! Hit refresh to get a new selection of news, or click on something to read the full article.
                </p>
                <p>
                    Made with &#128150; 18 months into the COVID-19 pandemic, because we all need some good news at the moment.
                </p>
                <p>
                    Icon made by <a href="https://www.freepik.com" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>
                </p>
            </div>
        </Modal>
    )
}
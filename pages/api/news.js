import { shuffle, isAnimalInText } from "../../fetchHelpers/utils";
import { newsSources } from "../../fetchHelpers/newsSources";
import { get } from "lodash";


export default function handler(req, res) {
  Promise.all([
    getNewsAndFormat(
      newsSources[0].baseUrl + newsSources[0].endpoint,
      newsSources[0].objectAccessString,
      newsSources[0].formatFunction,
    ),
    getNewsAndFormat(
      newsSources[1].baseUrl + newsSources[1].endpoint,
      newsSources[1].objectAccessString,
      newsSources[1].formatFunction,
    )
  ]).then(data => shuffle([].concat.apply([], data)))
    .then(data => res.status(200).json(data))
}

async function getNewsAndFormat(url, objectAccessString, formatFunction) {
  return fetch(url)
    .then(response => response.json())
    .then(data => {
      const formattedNews = get(data, objectAccessString)
      console.log(formattedNews.length);
      return formattedNews.map(formatFunction);
    })
    .then(data => data.filter(item => isAnimalInText(item.synopsis) || isAnimalInText(item.title)))
}

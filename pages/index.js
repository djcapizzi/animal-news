import useSWR from 'swr';
import NewsContainer from './components/newsContainer';
import AboutModal from './components/aboutModal';

import { Box, Grid, IconButton, Toolbar, Container } from '@material-ui/core';
import HelpIcon from '@material-ui/icons/Help';
import GitHubIcon from '@material-ui/icons/GitHub';
import { useState } from 'react';

const fetcher = (...args) => fetch(...args).then(res => res.json())

export default function Home() {
  const { data, error } = useSWR('/api/news', fetcher);
  const [open, setOpen] = useState(false);

  const handleOpen = () => {
    setOpen(true);
  }

  const handleClose = () => {
    setOpen(false);
  }

  if (data) {
    return (
      <div>
        <Box bgcolor="warning.main">
          <Grid
            container
            spacing={1}
            direction="column"
            alignItems="center"
          >
            <Grid item xs={12}>
              <Box fontSize="h2.fontSize">Furry Feed</Box>
            </Grid>   
            <Grid item xs={12}>
              <img src="pawprint.png" style={{ padding: "10px" }}/>
            </Grid>

          </Grid> 
          
        </Box>
        <Toolbar />

        <Container maxWidth="lg">
          <NewsContainer data={data} />

          <hr />

          <Box display="flex" flexDirection="row-reverse">
            <Box>
              <IconButton onClick={handleOpen}>
                <HelpIcon />
              </IconButton>
            </Box>
            <Box>
              <IconButton href="https://bitbucket.org/djcapizzi/animal-news/src/main/" target="_blank">
                <GitHubIcon />
              </IconButton>
            </Box>
          </Box>
        </Container>

        <AboutModal open={open} handleClose={handleClose} />

      </div>
    )
  }
  return (
    <div>
      loading...
    </div>
  )
}

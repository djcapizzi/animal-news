
export const newsSources = [
    {
        "name": "BBC",
        "baseUrl": "https://push.api.bbci.co.uk",
        "endpoint": "/batch?t=%2Fdata%2Fbbc-morph-lx-commentary-data-paged%2Fabout%2Facb3aada-8f46-456c-8e7c-faf92a68f06a%2FisUk%2Ffalse%2Flimit%2F20%2FnitroKey%2Flx-nitro%2FpageNumber%2F5%2Fversion%2F1.5.4?timeout=5",
        "objectAccessString": 'payload[0].body.results',
        "formatFunction": item => ({
            link: "https://www.bbc.co.uk" + item.url,
            image: {
                imgSrc: item.image.href,
                altText: item.image.altText
            },
            synopsis: item.summary,
            title: item.title,
            published: item.firstPublished
        })
    },
    {
        "name": "ABC",
        "baseUrl": "https://www.abc.net.au",
        "endpoint": "/news-web/api/loader/channelrefetch?name=PaginationArticles&documentId=7017602&offset=0&size=100",
        "objectAccessString": "collection",
        "formatFunction": item => ({
            link: "https://www.abc.net.au" + item.link.to,
            image: item.image,
            synopsis: item.synopsis,
            title: item.title.children,
            published: item.timestamp.dates.firstPublished.labelDate,
        })
    }
]
